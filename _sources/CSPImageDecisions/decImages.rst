..  Copyright (C) Mark Guzdial, Barbara Ericson, Briana Morrison
    Permission is granted to copy, distribute and/or modify this document
    under the terms of the GNU Free Documentation License, Version 1.3 or
    any later version published by the Free Software Foundation; with
    Invariant Sections being Forward, Prefaces, and Contributor List,
    no Front-Cover Texts, and no Back-Cover Texts.  A copy of the license
    is included in the section entitled "GNU Free Documentation License".

.. qnum::
    :start: 1
    :prefix: csp-15-1-

Using Decisions with Images
===========================

*Learning Objectives:*

- Show examples of using conditionals to modify images.
- Use conditionals to combine two pictures. 
- Use ``if`` and ``else`` with images. 
- Use multiple ``if`` statements with images.

.. raw:: html

    <img src="../_static/gal2.jpg" id="gal2.jpg">
	
We can create image effects by conditionally executing code.  In the code below
we will try to change the color of the women's shirt.  We will clear the red
value (set it to 0) for any pixel that has a red value greater than 200 and a
green value of less than 100 and a blue value of less than 100.   

.. activecode:: Color_Replace
    :tour_1: "Structural Tour"; 2: id1/line2; 5: id1/line5; 11-12: id1/lines11n12; 14: id1/line14; 17: id1/line17; 20: id1/line20; 23: id1/line23;
    :nocodelens:

    # Import the Image class from the PIL library 
    from PIL import Image 
    
    # Connect to an image file 
    img = Image.open('gal2.jpg')

    # Load pixels from the image 
    pixels = img.load()

    # Loop through all pixels
    for x in range(img.size[0]):
        for y in range(img.size[1]):
            # Get the red, green, and blue values of pixel
            r, g, b = pixels[x, y]

            # Values for the new color
            if r > 200 and g < 100 and b < 100:
            
               # Change the image
               pixels[x, y] = 0, g, b 
            
    # Show the changed image
    img.show()
    
What happens if we only test if red is greater than 200? Change the code above
to try it. 

Can you find values that work well to only change the shirt color and not
anything else?  

Remember that white light is a combination of red, green, and blue light with
each value at 255 for totally white light.  Try to change just the white
background to green.  
