.. qnum::
   :prefix: 3-11-
   :start: 1

Mixed Up Code Practice
------------------------------

Try to solve each of the following. Click the *Check Me* button to check each
solution.  You will be told if your solution is too short, has a block in the
wrong order, or you are using the wrong block.  Some of the problems have an
extra block that isn't needed in the correct solution.  Try to solve these on
your phone or other mobile device!

.. parsonsprob:: ch3ex1muc
   :adaptive:
   :noindent:

   The following program segment should print out the cost for each shirt if they are buy 2 and get the third free but were originally $45 each.  The blocks have been mixed up.  Drag the blocks from the left and put them in the correct order on the right.  Click the <i>Check Me</i> button to check your solution.</p>
   -----
   price = 45
   =====
   total_cost = price * 2
   =====
   price_per_shirt = total_cost / 3
   =====
   print(price_per_shirt)


.. parsonsprob:: ch3ex2muc
   :adaptive:
   :noindent:

   The following program segment should print out the cost per person for a dinner including the tip. But the blocks have been mixed up and include an extra block that isn't needed in the solution. But, the blocks have been mixed up and include an extra block that isn't needed in the solution. Drag the needed blocks from the left and put them in the correct order on the right.  Click the <i>Check Me</i> button to check your solution.</p>
   -----
   bill = 89.23
   =====
   tip = bill * 0.20
   =====
   total = bill + tip
   =====
   num_people = 3
   per_person_cost = total / num_people
   =====
   print(per_person_cost)
   =====
   print(perPersonCost) #distractor

.. parsonsprob:: ch3ex3muc
   :adaptive:
   :noindent:

   The following program segment should print the amount of punch left in a two gallon punch bowl if 12oz is poured into as many cups as possible. One gallon contains 128oz and the punch bowl is full. But, the blocks have been mixed up and include an extra block that isn't needed in the solution.  Drag the needed blocks from the left and put them in the correct order on the right.  Click the <i>Check Me</i> button to check your solution.</p>
   -----
   o_in_gallon = 128
   o_in_cup = 12
   =====
   total_punch = 2 * o_in_gallon
   =====
   amount_left = total_punch % o_in_cup
   =====
   print(amount_left)
   =====
   amount_left = total_punch / o_in_cup #distractor

.. parsonsprob:: ch3ex4muc
   :adaptive:
   :noindent:

   The following program segment should print the number of seconds in 5 days. But, the blocks have been mixed up and include an extra block that isn't needed in the solution.  Drag the needed blocks from the left and put them in the correct order on the right.  Click the <i>Check Me</i> button to check your solution.</p>
   -----
   s_in_min = 60
   m_in_hour = 60
   h_in_day = 24
   =====
   s_in_day = s_in_min * m_in_hour * h_in_day
   =====
   total = s_in_day * 5
   =====
   print(total)
   =====
   total = s_in_day / 5 #distractor

.. parsonsprob:: ch3ex5muc
   :adaptive:
   :noindent:

   The following program segment should print the number of months it would take you to save 500 if you make 50 a week. But, the blocks have been mixed up and include an extra block that isn't needed in the solution.  Drag the needed blocks from the left and put them in the correct order on the right.  Click the <i>Check Me</i> button to check your solution.</p>
   -----
   weekly_rate = 50
   goal = 500
   =====
   num_weeks = goal / weekly_rate
   =====
   num_months = num_weeks / 4
   =====
   print(num_months)
   =====
   num_weeks = weekly_rate / goal #distractor

.. parsonsprob:: ch3ex6muc
   :adaptive:
   :noindent:

   The following program segment should print the cost of a trip that is 200 miles when the price of gas is 2.20 and the miles per gallon is 42. But, the blocks have been mixed up and include an extra block that isn't needed in the solution.  Drag the needed blocks from the left and put them in the correct order on the right.  Click the <i>Check Me</i> button to check your solution.</p>
   -----
   miles = 200
   price = 2.20
   mpg = 42
   =====
   num_galls = miles / mpg
   =====
   cost = num_galls * price
   =====
   print(cost)
   =====
   print(total_cost) #distractor

.. parsonsprob:: ch3ex7muc
   :adaptive:
   :noindent:

   The following program segment should print how many miles you can go on half a tank of gas if the miles per gallon is 26 and your tank holds 15 gallons. But, the blocks have been mixed up and include an extra block that isn't needed in the solution.  Drag the needed blocks from the left and put them in the correct order on the right.  Click the <i>Check Me</i> button to check your solution.</p>
   -----
   mpg = 26
   tank_holds = 15
   =====
   num_galls = tank_holds / 2
   =====
   miles = num_galls * mpg
   =====
   print(miles)
   =====
   mpg = 15
   tank_holds = 26 #distractor

.. parsonsprob:: ch3ex8muc
   :adaptive:
   :noindent:

   The following program segment should print how many chicken wings you can buy with $3.50 if the wings are $.60 each. But, the blocks have been mixed up and include an extra block that isn't needed in the solution.  Drag the needed blocks from the left and put them in the correct order on the right.  Click the <i>Check Me</i> button to check your solution.</p>
   -----
   cost = 0.6
   money = 3.5
   =====
   num_wings = money / cost
   =====
   print(num_wings)
   =====
   print(NumWings) #distractor

.. parsonsprob:: ch3ex9muc
   :adaptive:
   :noindent:

   The following program segment should print how much you will have to pay for an item that is 60% off the original price of $52.99. But, the blocks have been mixed up and include an extra block that isn't needed in the solution.  Drag the needed blocks from the left and put them in the correct order on the right.  Click the <i>Check Me</i> button to check your solution.</p>
   -----
   price = 52.99
   discount = 0.6
   =====
   savings = price * discount
   =====
   final_price = price - savings
   =====
   print(final_price)
   =====
   final_price = price - discount #distractor

.. parsonsprob:: ch3ex10muc
   :adaptive:
   :noindent:

   The following program segment should print how much each pair of shorts cost when they are buy 2 and get the third free.  The shorts are originally $39.99 each. But, the blocks have been mixed up and include an extra block that isn't needed in the solution.  Drag the needed blocks from the left and put them in the correct order on the right.  Click the <i>Check Me</i> button to check your solution.</p>
   -----
   price = 39.99
   =====
   price_for_two = price * 2
   =====
   item_price = price_for_two / 3
   =====
   print(item_price)
   =====
   item_price = price_for_two / 2 #distractor
