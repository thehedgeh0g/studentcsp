..  Copyright (C)  Mark Guzdial, Barbara Ericson, Briana Morrison
    Permission is granted to copy, distribute and/or modify this document
    under the terms of the GNU Free Documentation License, Version 1.3 or
    any later version published by the Free Software Foundation; with
    Invariant Sections being Forward, Prefaces, and Contributor List,
    no Front-Cover Texts, and no Back-Cover Texts.  A copy of the license
    is included in the section entitled "GNU Free Documentation License".
    
.. |audiobutton| image:: Figures/start-audio-tour.png
    :height: 20px
    :align: top
    :alt: audio tour button

.. 	qnum::
	:start: 1
	:prefix: csp-9-3-
	
.. highlight:: python 
   :linenothreshold: 4

Mirroring Text
===============

What happens if we add the letter to *both* sides of the new string that we're
making?

.. activecode:: Copy_Mirror
    :tour_1: "Lines of code"; 2: strR3/line2; 5: strR3/line5; 8: strR3/line8; 10: strR3/line10; 13: strR3/line13;

    # Step 1: Initialize accumulator 
    new_string = ""

    # Step 2: Get data
    phrase = "This is a test"

    # Step 3: Loop through the data
    for letter in phrase:
      	# Step 4: Accumulate
      	new_string = letter + new_string + letter

    # Step 5: Process result
    print(new_string)

Try changing the phrase and see what effects you can generate.

.. mchoice:: 9_3_1_Copy_Mirror_Q1
   :answer_a: Make the phrase <code>"Time to panic!"</code>
   :answer_b: Change the <code>new_string</code> in line 1 to <code>"!"</code> instead of <cod>""</code>
   :answer_c: Change the right hand side of line 4 to <code>letter + "!" + new_string + letter</code>
   :answer_d: Change the right hand side of line 4 to <code>letter + new_string + "!" + letter</code>
   :correct: b
   :feedback_a: That would give us <code>!cinaP ot emiTTime to Panic!</code>.
   :feedback_b: We can start our accumulator with something in it.
   :feedback_c: That would give us <code>!!c!i!n!a!P! !o!t! !e!m!i!T!Time to Panic!</code> -- exclamation points between the letters in the first half of the mirror.
   :feedback_d: That would give us <code>!cinaP ot emiT!T!i!m!e! !t!o! !P!a!n!i!c!!</code> -- exclamation points between the letters in the second half of the mirror.

   Change the mirroring program to mirror the phrase ``"Time to Panic"`` with a single exclamation point in the middle, to make the printed words look like this: ``cinaP ot emiT!Time to Panic``.  How do you do it?

The accumulator doesn't have to be set to be an empty string.  You can put
something in the accumulator, and then it will show up in the middle of the
mirrored phrase.

.. codelens:: Char_In_Middle

    new_string = "!"
    phrase = "We're off to see the Wizard!"
    for letter in phrase:
        new_string = letter + new_string + letter
    print(new_string)

.. mchoice:: 9_3_2_Count_Exclamations_Q1
   :answer_a: One
   :answer_b: Two
   :answer_c: Three
   :answer_d: Four
   :correct: a
   :feedback_a: There is just the one in the accumulator to start.
   :feedback_b: If we just mirrored the string, there would be only two.  But we are mirroring with something in the accumulator.
   :feedback_c: That is true at the end, but not when letter contains the first letter of <code>"Wizard"</code>
   :feedback_d: At most, there will be three in <code>new_string</code>.

   When the variable ``letter`` contains the ``"W"`` from ``Wizard``, how many exclamation points are in ``new_string``?

.. parsonsprob:: 9_3_3_Palindrome

   <p>The phrase <code>"A but tuba"</code> is a <b>palindrome</b>.  The letters are the same forward and backward.  The below program generates the output: <code>"abut tub a<=>a but tuba"</code>  Put the lines in the right order with the right indentation.</p>
   -----
   new_str = "<=>"
   phrase = "a but tuba"
   =====
   for char in phrase:
   =====
       new_str = char + new_str + char
   =====
   print(new_str)

