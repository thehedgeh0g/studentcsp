..  Copyright (C)  Mark Guzdial, Barbara Ericson, Briana Morrison, Jeff Elkner
    Permission is granted to copy, distribute and/or modify this document
    under the terms of the GNU Free Documentation License, Version 1.3 or
    any later version published by the Free Software Foundation; with
    Invariant Sections being Forward, Prefaces, and Contributor List,
    no Front-Cover Texts, and no Back-Cover Texts.  A copy of the license
    is included in the section entitled "GNU Free Documentation License".
    
.. |audiobutton| image:: Figures/start-audio-tour.png
    :height: 20px
    :align: top
    :alt: audio tour button

.. qnum::
    :start: 1
    :prefix: csp-11-1-

Using Repetition with Images
============================

*Learning Objectives:*

- Use ``for`` loops to repeat actions on all pixels in a picture.
- Understand the pattern (the steps) used in modifying all the pixels in a
  picture.

.. index::
    single: images
    pair: statements; for

.. index::
    single: pixel
    single: picture
    single: color

Pictures on a computer are broken up into little bits called **pixels**, for
*picture* (pix) *elements* (els).  These are laid out on a grid, from left to
right (horizontal or **x** dimension) and top to bottom (vertical or **y**
dimension).

.. figure:: Figures/grid.png
    :align: center
    :alt: A grid with horizontal (x) and vertical (y) dimensions 
    :figclass: align-center
    
    Figure: A grid with horizontal (x) and vertical (y) dimensions

Pixels are quite small.  Even this small picture below has 180 columns and 240
rows of pixels:

.. raw:: html

    <img src="../_static/arch.jpg" id="arch.jpg">

Each pixel has a color associated with it: An amount of redness, an amount of
greenness, and an amount of blueness.  Let's remove the red from this picture.
Now, there are lot of lines in the program below, but fortunately, you can
ignore most of them. The Audio Tour explains the important lines.  Press
|audiobutton| to hear the audio tour explanation.  When you run this program it
may take several minutes to show the changed picture. 


.. activecode:: Image_Remove_Red
    :tour_1: "Important Lines Tour"; 1: timg3/line1; 4: timg3/line4; 7: timg3/line7; 8: timg3/line8; 9: timg3/line9; 12-13: timg3/lines12n13; 14: timg3/line14; 15: timg3/line15; 17: timg3/line17;
    :nocodelens:

    from PIL import Image

    # Create an image from a file
    img = Image.open('arch.jpg')

    # Load pixels and get dimensions
    pixels = img.load()
    width = img.size[0]
    height = img.size[1]

    # Loop through each pixel
    for col in range(width):
	for row in range(height):
	    r, g, b = pixels[col, row]   # Get color
	    pixels[col, row] = (r, 0, b) # Set green to 0

    img.show() 

The program above can take several minutes to execute on the ``arch.jpg``
picture.  But we're not stuck using just the arch image.  We can use smaller
images which will execute more quickly.

A Library of Images
===================

Here are some other small images that you can use.  Modify the program above to
try out the code on some of these other images by changing line 4 above. 

You can actually run this code on any image on the web by specifying the image
url.  We recommend using small images since larger images will take more time
to process.

.. raw:: html

   <table>
   <tr><td>beach.jpg</td><td>baby.jpg</td><td>vangogh.jpg</td><td>swan.jpg</td></tr>
   <tr><td><img src="../_static/beach.jpg" id="beach.jpg"></td><td><img src="../_static/baby.jpg" id="baby.jpg"></td><td><img src="../_static/vangogh.jpg" id="vangogh.jpg"></td><td><img src="../_static/swan.jpg" id="swan.jpg"></td></tr>
   </table>
   <table>
   <tr><td>puppy.jpg</td><td>kitten.jpg</td><td>girl.jpg</td><td>motorcycle.jpg</td></tr>
   <tr><td><img src="../_static/puppy.jpg" id="puppy.jpg"></td><td><img src="../_static/kitten.jpg" id="kitten.jpg"></td><td><img src="../_static/girl.jpg" id="girl.jpg"></td><td><img src="../_static/motorcycle.jpg" id="motorcycle.jpg"></td></tr>
   </table>
   <table>
   <tr><td>gal1.jpg</td><td>guy1.jpg</td><td>gal2.jpg</td></tr>
   <tr><td><img src="../_static/gal1.jpg" id="gal1.jpg"></td><td><img src="../_static/guy1.jpg" id="guy1.jpg"></td><td><img src="../_static/gal2.jpg" id="gal2.jpg"></td></tr>
   </table>
