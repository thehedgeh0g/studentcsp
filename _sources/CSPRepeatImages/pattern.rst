..  Copyright (C)  Mark Guzdial, Barbara Ericson, Briana Morrison, Jeff Elkner
    Permission is granted to copy, distribute and/or modify this document
    under the terms of the GNU Free Documentation License, Version 1.3 or
    any later version published by the Free Software Foundation; with
    Invariant Sections being Forward, Prefaces, and Contributor List,
    no Front-Cover Texts, and no Back-Cover Texts.  A copy of the license
    is included in the section entitled "GNU Free Documentation License".
    
.. |audiobutton| image:: Figures/start-audio-tour.png
    :height: 20px
    :align: top
    :alt: audio tour button

.. qnum::
    :start: 1
    :prefix: csp-11-3-

A Pattern for Image Processing
==============================

As we have seen with turtles and words, there are some general patterns in the
programs that we write.  With turtles, there was a polygon pattern (based on
the Total Turtle Trip Theorem).  When working with words and numbers, we used
the accumulator pattern.

The image processing pattern is shown in the program below.  This program
changes the red to the original green, the green to the original blue, and the
red to the original green.  But, mostly we are trying to describe a pattern
that you can use to create many image effects.

.. raw:: html

   <img src="../_static/beach.jpg" id="beach.jpg">

.. activecode:: Image_Pattern
    :tour_1: "Important Lines Tour"; 2: timg4/line2; 5: timg4/line5; 8: timg4/line8; 11-12: timg4/lines11n12; 15: timg4/line15; 18: timg4/line18; 21: timg4/line21;
    :nocodelens:

    # Step 1: Load Image from the image library 
    from PIL import Image
    
    # Step 2: Link to the image file
    img = Image.open("beach.jpg")

    # Step 3: Load the pixels from the image
    pixels = img.load()

    # Step 4: Loop through the pixels
    for col in range(img.size[0]):
        for row in range(img.size[1]):
        
            # Step 4: Get the data
            r, g, b = pixels[col, row]
            
            # Step 5: Modify the color
            pixels[col, row] = (g, b, r)

    # Step 7: Show the result
    img.show()


Here are our seven steps:

* **Step 1**: *IMPORT Image from the Python Image Library (PIL)*. We need to
  import `Image` from `PIL` to use it to load an image.
* **Step 2**: *Pick the image*. We link a variable to a particular image from
  our library by creating an `Image` object and using it to open the image
  file.
* **Step 3**: *Load the pixels*. This loads the matrix of pixels that make up
  an image.
* **Step 4**: *Loop through the pixels*. This example gets *every* pixel in the
  image one at a time by looping over each column and then each row therein.
* **Step 5**: *Get the data*.  Pixels are a combination of three values - red,
  green, and blue. 
* **Step 6**: *Modify the color*. This is the part that you will most often
  change.  Here's where you can change the red, green, and/or blue values of a
  pixel.
* **Step 7**: *Show the result*.  This will draw the changed image.
